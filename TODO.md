# Discord API v8

- [x] Objects;
    - [ ] Add user-only objects;
- [x] Http API:
    - [x] Constructors;
    - [x] Channel methods:
        - [ ] Check that `before`/`after`/`limit` are passed properly (as Query string).
    - [x] Emoji;
    - [x] Guild:
        - [ ] `listGuildMembers` and `getGuild` w/ query params;
        - [ ] Implement `Create Guild Channel`, `Add Guild Member`;
        - [ ] Properly implement `Guild Prune` methods;
    - [x] Invite;
    - [x] Template;
    - [x] User;
        - [ ] `getCurrentUserGuilds` w/ query params;
    - [ ] Test Http Api.
- [x] Gateway (Realtime) API:
    - [ ] Implement custom gateway selection (via http api);
    - [x] Resuming;
    - [ ] Sharding;
    - [x] Events;
- [x] Voice API;
- [ ] Webhooks;
- [ ] Interactions (slash commands);
- [ ] Merge MPFL into utils library;

# Other

- [ ] v8 RoleTags premium_subscriber is undefined!