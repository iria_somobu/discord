# Discord API

 - Lightweight (compiled jar is about 100 kbytes large);
 - Stupid and barebones;
 - Java 7 compatible.


## Usage
Very quick reference:
- `DiscordHttp` wraps Discord [HTTP/REST API](https://discordapp.com/developers/docs/reference);
- `DiscordGateway` establishes connection with Discord's [realtime Gateway](https://discordapp.com/developers/docs/topics/gateway);
- `DiscrodApiVoice` provides user-friendly voice API;
- `DiscrodApiVoiceBase` provides low-level voice API.


### Http API

```java
// This is how you call http api methods
DiscordHttp api = new DiscordHttp("BOT TOKEN HERE");
api.createMessage("YOUR CHANNEL ID", "Hello world!");
```


### Realtime API

```java
// As for realtime Gateway, you probably want to set own DiscordEventsListener
DiscordGateway gateway = new DiscordGateway("BOT TOKEN HERE");
gateway.setDiscordListener(new DiscordGateway.EventsListener() {

    // This listener provides a lot of overridable methods
    // representing gateway's events
    // We'll override just this one to get new messages

    @Override
    public void onMessageCreate(Message message) {
        System.out.println(message.content);
    }
}
// And don't forget to start realtime
gateway.start();

// For a sake of simplicity, we'll just wait for a bit to get Realtime gateway connected
// You might want to implement onReady() in our `DiscordEventsListener`
// or watch for `DiscordGateway`'s state changes using `gateway.setStateListener(...)`
Thread.sleep(2 * 1000);

// ...

// Wait for a while and then stop realtime gateway
Thread.sleep(75 * 60 * 1000);
gateway.stop();

```


### Voice API

First of all, you have to load `jnopus` library. You can find prebuilt libs on [github page of libjitsi](https://github.com/jitsi/libjitsi/tree/master/lib/native).

Then, you can use `DiscordApiVoice` (assuming `gateway` is already-connected instance of `DiscordGateway`):

```java
DiscordApiVoice vc = new DiscordApiVoice(
    gateway /* You have to pass connected Realtime gateway here */, 
    null /* We're not to listen for any voice event */);

// `connect()` is a blocking method that waits till voice api is connected properly
vc.connect("GUILD ID", "SERVER ID", /*Not mute*/ false, /*Not deaf*/ false);

// You can play audio from plain `AudioInputStream`
AudioInputStream audioStream = ...;
vc.startPlayback(audioStream, new DiscordApiVoice.IPlaybackListener() {
    
    @Override
    public void onError(Exception e) {
        System.out.println("Playback error");
    }

    @Override
    public void onFinished() {
        // This callback method will be called when playback is finished
        // -- either normally or by an error
        System.out.println("Playback finished");
    }
});
```


## Download
Gradle:
```gradle
// Repo
repositories {
    maven { url "https://gitlab.com/iria_somobu/m2/-/raw/master/" }
}

// Library
dependencies {
    implementation 'somobu:discord:2.2.0'
}
```


## Dependencies
- This library internally uses own copy of [Opus encoder/decoder for Java written by Jitsi team](https://github.com/jitsi/libjitsi/tree/master/src/org/jitsi/impl/neomedia/codec/audio/opus);
- For other dependencies, see `build.gradle` file.


## License
```
GNU GENERAL PUBLIC LICENSE
Version 3, 29 June 2007

Copyright (C) 2019  Ilya Somov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
```

