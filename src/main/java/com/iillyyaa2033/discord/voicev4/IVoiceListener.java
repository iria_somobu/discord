package com.iillyyaa2033.discord.voicev4;

import com.iillyyaa2033.discord.voicev4.objects.VoiceSpeaking;

public interface IVoiceListener {

    void onVoiceReady();
    void onError(Exception ex);
    void onClose(boolean remote, int code, String reason);

    void onSpeaking(VoiceSpeaking speaking);
    void onClientDisconnect(String user_id);
}
