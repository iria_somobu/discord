package com.iillyyaa2033.discord.voicev4;

public interface IPlaybackListener {

    /**
     * Fired when something bad happened
     * <p>
     * Note: this not necessary means that player stops working
     *
     * @param e error
     */
    void onError(Exception e);

    /**
     * When playback finished or stops playing in any means (i.e. crashed)
     */
    void onFinished();

}
