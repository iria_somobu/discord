package com.iillyyaa2033.discord.voicev4.objects;

public class VoiceSessionDescription {

    public String video_codec;
    public String mode;
    public byte[] secret_key;
    public String media_session_id;
    public String audio_codec;

}
