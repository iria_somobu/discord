package com.iillyyaa2033.discord.voicev4.objects;

import com.iillyyaa2033.discord.v8.objects.GuildMember;

public class VoiceState  {

    public String guild_id;
    public String channel_id;
    public String user_id;
    public GuildMember member;
    public String session_id;
    boolean deaf;
    boolean mute;
    boolean self_deaf;
    boolean self_mute;
    public boolean self_steam;
    boolean suppress;

}
