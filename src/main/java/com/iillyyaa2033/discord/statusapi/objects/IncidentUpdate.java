package com.iillyyaa2033.discord.statusapi.objects;

public class IncidentUpdate {

    public String id;
    public String status;
    public String body;
    public String incident_id;
    public String created_at;
    public String updated_at;
    public String display_at;
    public IncidentComponent[] affected_components;
    public String custom_tweet;
    public String tweet_id;

}
