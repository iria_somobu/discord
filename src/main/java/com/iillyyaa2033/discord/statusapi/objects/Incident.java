package com.iillyyaa2033.discord.statusapi.objects;

public class Incident {

    public String id;
    public String page_id;
    public String name;
    public String status;
    public String started_at;
    public String created_at;
    public String updated_at;
    public String monitoring_at;
    public String resolved_at;
    public String impact;
    public String shortlink;

    public IncidentUpdate[] incident_updates;
    public IncidentComponent[] components;

}
