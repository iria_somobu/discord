package com.iillyyaa2033.discord.statusapi.objects;

public class IncidentPage {

    public String id;
    public String name;
    public String url;
    public String time_zone;
    public String updated_at;

}
