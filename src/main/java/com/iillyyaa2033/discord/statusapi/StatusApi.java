package com.iillyyaa2033.discord.statusapi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iillyyaa2033.discord.statusapi.objects.IncidentsSummary;
import com.iillyyaa2033.utils.StreamUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class StatusApi {

    private static final String genericUserAgent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1";
    private static final String endpointUrl = "https://status.discordapp.com/api/v2/";

    private final Gson gson = new GsonBuilder().create();

    public IncidentsSummary getIncidents() {
        try {
            return gson.fromJson(internalHttpRequest("GET", "incidents/unresolved.json", null), IncidentsSummary.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String internalHttpRequest(final String method, final String sub_path, final String data) throws IOException {
        String url = endpointUrl + sub_path;
        boolean needOutput = (data != null);

        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
        http.setRequestMethod(method);
        http.setRequestProperty("User-Agent", genericUserAgent);
        http.setDoOutput(needOutput);
        http.setDoInput(true);

        byte[] bytes = new byte[0];
        if (needOutput) {
            bytes = data.getBytes(StandardCharsets.UTF_8);
            http.setFixedLengthStreamingMode(bytes.length);
        }

        http.connect();

        if (needOutput) {
            try (OutputStream os = http.getOutputStream()) {
                os.write(bytes);
                os.flush();
            }
        }

        int rc = http.getResponseCode();

        // Reading server's answer
        if (200 <= rc && rc <= 299) {
            return StreamUtils.convertStreamToString(http.getInputStream());
        } else {
            String serverAnswer = StreamUtils.convertStreamToString(http.getErrorStream());
            throw new IOException(method + " " + sub_path + " " + serverAnswer);
        }
    }

}
