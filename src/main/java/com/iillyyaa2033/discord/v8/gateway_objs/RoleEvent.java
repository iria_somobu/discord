package com.iillyyaa2033.discord.v8.gateway_objs;

import com.iillyyaa2033.discord.v8.objects.Role;

public class RoleEvent {

    public String guild_id;

    /**
     * Present in role create and role update events
     */
    public Role role;

    /**
     * Present in role delete event
     */
    public String role_id;

}
