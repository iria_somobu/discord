package com.iillyyaa2033.discord.v8.objects;

public class Role {

    public String id, name;
    public int color;
    public boolean hoist;
    public int position;
    public String permissions;
    public boolean managed, mentionable;
    public RoleTags tags;

}
