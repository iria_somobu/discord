package com.iillyyaa2033.discord.v8.objects;

public class Connection {

    public String id, name, type;
    public boolean revoked;
    public Integration[] integrations;
    public boolean verified, friend_sync, show_activity;
    public int visibility;

}
