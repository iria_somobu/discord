package com.iillyyaa2033.discord.v8.objects;

public class GuildCreate {

    public String name;
    public String region;
    public ImageData icon;
    public int verification_level;
    public int default_message_notifications;
    public int explicit_content_filter;
    public Role[] roles;
    public Channel[] channels;
    public String afk_channel_id;
    public int afk_timeout;
    public String system_channel_id;
    public String system_channel_flags;

}
