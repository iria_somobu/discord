package com.iillyyaa2033.discord.v8.objects;

public class MessageReference {

    public String message_id, channel_id, guild_id;
    public boolean fail_if_not_exists;
}
