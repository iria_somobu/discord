package com.iillyyaa2033.discord.v8.objects;

public class MessageMultipart {

    public String content;
    public String nonce;
    public boolean tts;
    public String file;

    /**
     * Convert MessageJson to string and past here if you want rich mesage to be sent
     */
    public String payload_json;

}
