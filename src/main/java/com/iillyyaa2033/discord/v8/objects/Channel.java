package com.iillyyaa2033.discord.v8.objects;

public class Channel {

    public String id;
    public int type;
    public String guild_id;
    public int position;
    public PermissionOverwrite[] permission_overwrites;
    public String name;
    public String topic;
    public boolean nsfw;
    public String last_message_id;
    public int bitrate;
    public int user_limit;
    public int rate_limit_per_user;
    public User[] recipients;
    public String icon;
    public String owner_id;
    public String application_id;
    public String parent_id;
    public String last_pin_timestamp;


    public String rtc_region;
    public int video_quality_mode;

    public ChannelModification asMod() {
        ChannelModification mod = new ChannelModification();
        mod.name = name;
        mod.type = type;
        mod.position = position;
        mod.topic = topic;
        mod.nsfw = nsfw;
        mod.rate_limit_per_user = rate_limit_per_user;
        mod.bitrate = bitrate;
        mod.user_limit = user_limit;
        mod.permission_overwrites = permission_overwrites;
        mod.parent_id = parent_id;
        mod.rtc_region = rtc_region;
        mod.video_quality_mode = video_quality_mode;
        return mod;
    }
}
