package com.iillyyaa2033.discord.v8.objects;

public class Embed {

    public String title;
    public String type;
    public String description;
    public String timestamp;
    public int color;
    public EmbedFooter footer;
    public EmbedImage image;
    public EmbedThumbnail thumbnail;
    public EmbedVideo video;
    public EmbedProvider provider;
    public EmbedAuthor author;
    public EmbedFields[] fields;

}
