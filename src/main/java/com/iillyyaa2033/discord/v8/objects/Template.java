package com.iillyyaa2033.discord.v8.objects;

public class Template {

    public String code, name, description;
    public int usage_count;
    public String creator_id;
    public User creator;
    public String created_at, updated_at, source_guild_id;
    public Guild serialized_source_guild;
    public boolean is_dirty;

}
