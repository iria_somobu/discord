package com.iillyyaa2033.discord.v8.objects;

public class GuildWidgetJson {

    public String id;
    public String name;
    public String instant_invite;
    public Channel[] channels;
    // TODO: members // public Member members;
    public int presence_count;
}
