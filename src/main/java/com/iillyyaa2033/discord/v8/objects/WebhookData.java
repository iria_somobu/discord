package com.iillyyaa2033.discord.v8.objects;

public class WebhookData {

    public String content, username, avatar_url;
    public boolean tts;
    public Embed[] embeds;
    public AllowedMentions allowed_mentions;
    public MessageComponent components;
    public Attachment[] attachments;
    public int flags;
    public String thread_name;

}
