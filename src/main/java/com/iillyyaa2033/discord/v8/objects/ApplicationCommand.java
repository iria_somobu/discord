package com.iillyyaa2033.discord.v8.objects;

public class ApplicationCommand {

    public String id;
    public int type;
    public String application_id;
    public String guild_id;
    public String name;
    public String description;
    public Option[] options;
    public boolean default_permission;

    public static class Option {

        public static final int SUB_COMMAND = 1;
        public static final int SUB_COMMAND_GROUP = 2;
        public static final int STRING = 3;
        public static final int INTEGER = 4;
        public static final int BOOLEAN = 5;
        public static final int USER = 6;
        public static final int CHANNEL = 7;
        public static final int ROLE = 8;
        public static final int MENTIONABLE = 9;
        public static final int NUMBER = 10;
        public static final int ATTACHMENT = 11;

        public int type;
        public String name;
        public String description;
        public boolean required;
        public Choice[] choices;
        public Option[] options;

    }

    public static class Choice {

        public String name;
        public String value;

    }
}
