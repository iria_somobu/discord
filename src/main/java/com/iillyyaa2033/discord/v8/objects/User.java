package com.iillyyaa2033.discord.v8.objects;

import com.iillyyaa2033.discord.UserOnly;

public class User {

    public String id;
    public String username;
    public String discriminator;
    public String avatar;
    public String avatar_decoration;

    public boolean bot;
    public boolean system;
    public boolean mfa_enabled;
    public String locale;
    public boolean verified;
    public String email;
    public long flags;
    public long premium_type;
    public long public_flags;

    public int accent_color;
    public String banner;
    public String banner_color;
    public String bio;

    public @UserOnly int purchased_flags;
    public @UserOnly boolean premium;
    public @UserOnly String phone;
    public @UserOnly boolean nsfw_allowed;
    public @UserOnly boolean mobile, desktop;

}
