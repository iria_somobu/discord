package com.iillyyaa2033.discord.v8.objects;

public class Activity {

    public String name;
    public int type;
    public String url;
    public int created_at;
    public Timestamp[] timestamps;
    public String application_id, details, state;
    public Emoji emoji;
    public Party party;
    public Assets assets;
    public Secrets secrets;
    public boolean instance;
    public int flags;
    public Button[] buttons;

    public static class Timestamp {
        public long start;
        public long end;
    }

    public static class Party {
        public String id;
        public int[] size;
    }

    public static class Assets {
        public String large_image, large_text, small_image, small_text;
    }

    public static class Secrets {
        public String join, spectate, match;
    }

    public static class Button {
        public String label, url;
    }
}
