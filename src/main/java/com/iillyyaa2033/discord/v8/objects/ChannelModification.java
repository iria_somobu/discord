package com.iillyyaa2033.discord.v8.objects;

public class ChannelModification {

    public String name;
    public int type;
    public int position;
    public String topic;
    public boolean nsfw;
    public int rate_limit_per_user;
    public int bitrate;
    public int user_limit;
    public PermissionOverwrite[] permission_overwrites;
    public String parent_id;
    public String rtc_region;
    public int video_quality_mode;

}
