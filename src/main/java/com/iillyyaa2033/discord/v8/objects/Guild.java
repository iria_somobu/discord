package com.iillyyaa2033.discord.v8.objects;

import com.iillyyaa2033.discord.v8.gateway_objs.PresenceUpdate;

public class Guild {

    public String id, name, icon, icon_hash, splash, discovery_splash;
    public boolean owner;
    public String owner_id, permissions, region, afk_channel_id;
    public int afk_timeout;
    public boolean widget_enabled;
    public String widget_channel_id;
    public int verification_level, default_message_notifications, explicit_content_filter;
    public Role[] roles;
    public Emoji[] emojis;
    public String[] features;
    public int mfa_level;
    public String application_id, system_channel_id;
    public int system_channel_flags;
    public String rules_channel_id, joined_at;
    public boolean large, unavailable;
    public int member_count;
    public VoiceState[] voice_states;
    public GuildMember[] members;
    public Channel[] channels;
    public PresenceUpdate[] presences;
    public int max_presences, max_members;
    public String vanity_url_code, description, banner;
    public int premium_tier, premium_subscription_count;
    public String preferred_locale, public_updates_channel_id;
    public int max_video_channel_users, approximate_member_count, approximate_presence_count;
    public WelcomeScreen welcome_screen;
    public boolean nsfw;

//    public Object hub_type;
    public boolean lazy;
//    public Object[] stage_instances;
//    public Object[] threads;
//    public Object[] embedded_activities;
//    public Object[] guild_scheduled_events;
    public int nsfw_level;
//    public Object[] stickers;
    public boolean premium_progress_bar_enabled;
}
