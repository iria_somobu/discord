package com.iillyyaa2033.discord.v8.objects;

public class GuildModify {

    public String name;
    public String region;
    public int verification_level;
    public int default_message_notifications;
    public int explicit_content_filter;
    public String afk_channel_id;
    public int afk_timeout;
    public ImageData icon;
    public String owner_id;
    public ImageData splash, discovery_splash, banner;
    public String system_channel_id;
    public int system_channel_flags;
    public String rules_channel_id;
    public String public_updates_channel_id;
    public String preferred_locale;
    public String[] features;
    public String description;

}
