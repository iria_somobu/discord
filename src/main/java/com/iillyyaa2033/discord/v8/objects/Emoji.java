package com.iillyyaa2033.discord.v8.objects;

public class Emoji {

    public String id;
    public String name;
    public Role[] roles;
    public User user;
    public boolean require_colons;
    public boolean managed;
    public boolean animated;
    public boolean available;

}
