package com.iillyyaa2033.discord.v8.objects;

import com.google.gson.JsonElement;

public class Interaction {

    public String id, application_id;
    public int type;
    public ApplicationCommandInteractionData data;
    public String guild_id, channel_id;
    public GuildMember member;
    public User user;
    public String token;
    public int version;

    public static class ApplicationCommandInteractionData {
        public String id, name;
        public ApplicationCommandInteractionDataResolved resolved;
        public ApplicationCommandInteractionDataOption[] options;
    }

    public static class ApplicationCommandInteractionDataResolved{
        // TODO: dunno what to do here
    }

    public static class ApplicationCommandInteractionDataOption {
        public String name;
        public int type;
        public JsonElement value;
        public ApplicationCommandInteractionDataOption[] options;
    }
}
