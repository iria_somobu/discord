package com.iillyyaa2033.discord.v8.objects;

import com.iillyyaa2033.discord.UserOnly;

@UserOnly
public class UserSettings {

    public boolean inline_attachment_media, show_current_game, view_nsfw_guilds, enable_tts_command, render_reactions;
    public boolean gif_auto_play, stream_notifications_enabled, animate_emoji, detect_platform_accounts;
    public boolean default_guilds_restricted, allow_accessibility_detection, native_phone_integration_enabled;
    public boolean contact_sync_enabled, disable_games_tab, inline_embed_media, developer_mode, render_embeds;
    public boolean message_display_compact, convert_emoticons, passwordless;

    public int afk_timeout, explicit_content_filter, timezone_offset, friend_discovery_flags, animate_stickers;

    public String status, theme, locale;

    public String[] guild_positions;

    CustomStatus custom_status;

    Object friend_source_flags;
    Object[] restricted_guilds;

    @UserOnly
    public static class CustomStatus {
        public String text, expires_at, emoji_name, emoji_id;
    }

    @UserOnly static class GuildFolder {
        public String name, id, color;
        public String[] guild_ids;
    }
}
