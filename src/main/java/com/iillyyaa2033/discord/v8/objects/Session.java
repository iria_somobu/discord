package com.iillyyaa2033.discord.v8.objects;

public class Session {

    public String status;
    public String session_id;
    public ClientInfo client_info;
    public Activity[] activities;

    public static class ClientInfo {
        public int version;
        public String os, client;
    }
}
