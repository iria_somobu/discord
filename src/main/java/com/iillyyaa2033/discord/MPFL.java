package com.iillyyaa2033.discord;

import com.iillyyaa2033.utils.NetworkUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URLConnection;

/**
 * Temporary multipart file upload helper class
 */
public class MPFL {

    public static boolean DEBUG_ATTACHMENT_SENDING = false;

    /**
     * Send file as http multipart/form-data
     *
     * @param httpConn  connection write to
     * @param fieldName name of form filed
     * @param path      location of file to send
     * @throws IOException when something went wrong
     */
    public static void multipartFileUpload(HttpURLConnection httpConn, String json_data, String fieldName, String path, NetworkUtils.IUploadListener listener) throws IOException {
        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach...");

        // VARIABLES
        String boundary = "===" + System.currentTimeMillis() + "==="; // creates a unique boundary based on time stamp
        String LINE_FEED = "\r\n";
        OutputStream outputStream;
        PrintWriter writer;

        // INIT
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true);    // indicates POST method
        httpConn.setDoInput(true);
        httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
        outputStream = httpConn.getOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(outputStream), true);

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach: inited");

        // FORM

        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"payload_json\"").append(LINE_FEED);
        writer.append("Content-Type: text/plain; charset=utf8").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(json_data).append(LINE_FEED);
        writer.flush();

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach: form written");

        // FILE
        File uploadFile = new File(path);
        String fileName = uploadFile.getName();
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"").append(LINE_FEED);
        writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName)).append(LINE_FEED);
        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);
        long size = uploadFile.length();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        int i = 0;
        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Writing");
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            i++;
            outputStream.write(buffer, 0, bytesRead);
            if (listener != null) listener.progress(buffer.length * i, size);
        }
        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Flushing(1)");
        outputStream.flush();
        inputStream.close();
        writer.append(LINE_FEED);

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Flushing(2)");
        writer.flush();

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach: file written");

        // SEND
        writer.append(LINE_FEED).flush();
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.close();

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach: message sent");
    }

}
